//AJAX використовується для обміну інформацією між сервером та javascript, js може надсилати запити на сервер, отримувати від нього певні дані, надсилати дані на сервер
fetch('https://ajax.test-danit.com/api/swapi/films')
    .then((response) => {
        return response.json();
    })
    .then((films) => {
        films.forEach((film) => {
            const li = document.createElement("li");
            const episodeNumber = document.createElement("p");
            episodeNumber.textContent = `Episode ${film.id}`;
            const episodeName = document.createElement("p");
            episodeName.textContent = film.name;
            const episodeDescription = document.createElement("p");
            episodeDescription.textContent = film.openingCrawl;
            li.append(episodeNumber);
            li.append(episodeName);
            li.append(episodeDescription);
            const ul = document.getElementById("filmsList");
            ul.append(li);
            const characters = film.characters;
            characters.forEach((char) => {
                fetch(char)
                    .then((response) => {
                        return response.json();
                    })
                    .then((character) => {
                        const episodeCharacter = document.createElement('p');
                        episodeCharacter.textContent = character.name;
                        li.append(episodeCharacter);
                    })
            })

        })
    });

